import os
# Production variable
WEBCOMICS_DIR= os.environ['WEBCOMICS_DIR']
FONTS_DIR = os.environ['FONTS_DIR']
FONTS_SYS_DIR = '/home/carrot/.fonts/peppercarrot'
RENDERFARM_SCRIPT = '/home/carrot/renderfarm-lite.py'
THEME_PATH='/home/carrot/render-theme'

# Testing variables
#WEBCOMICS_DIR = '/home/vincent/work/peppercarrot/webcomics'
#FONTS_DIR = '/home/vincent/work/peppercarrot/fonts'
#FONTS_SYS_DIR = '/home/vincent/.fonts/peppercarrot'
#RENDERFARM_SCRIPT = '/home/vincent/work/peppercarrot/tools/utils/renderfarm-lite.py'
#THEME_PATH='/home/vincent/work/peppercarrot/peppercarrot-render-theme'

# Common variables
WEBCOMICS_REPO = 'https://framagit.org/peppercarrot/webcomics.git'
GITLAB_API_URL = 'https://framagit.org/api/v4/projects/19067'
FONTS_REPO = 'https://framagit.org/peppercarrot/fonts.git'
OUTPUT_DIR = f"{WEBCOMICS_DIR}/outputs"
THEME_REPO='https://framagit.org/valvin/peppercarrot-render-theme.git'