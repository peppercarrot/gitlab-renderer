#!/usr/bin/python3
import requests
import os, shutil
import json
import conf


def get_opened_merged_request():
    r = requests.get(f"{conf.GITLAB_API_URL}/merge_requests?state=opened&sort=asc")
    merge_requests = []
    if r.status_code == 200:
        opened_mr = r.json()
        for mr in opened_mr:
            print(f"Processing MR {mr['id']} from {mr['author']['username']}")
            if mr['source_project_id'] == mr['target_project_id']:
                merge_request = {}
                merge_request['branch'] = mr['source_branch']
                merge_request['author'] = mr['author']['username']
                merge_request['title'] = mr['title']
                merge_requests.append(merge_request)
            else:
                print(f"{mr['author']['username']}'s Merge Request from another remote not yet supported")
                # TODO : get remote url from source branch and checkout it
    return merge_requests


def is_cache_present():
    if os.path.exists(conf.WEBCOMICS_DIR) and os.path.exists(conf.FONTS_DIR):
        return True
    else:
        return False

#FIXME: wrong function name
def update_repo():
    if is_cache_present():
        print('cache detected')
        os.system(f"rm -rf {conf.WEBCOMICS_DIR}/*/render")
        os.system(f"rm -rf {conf.OUTPUT_DIR}")
    else:
        print('no cache detected. cloning webcomics repo')
        os.system(f"git clone {conf.WEBCOMICS_REPO} {conf.WEBCOMICS_DIR}")


def update_font():
    if is_cache_present():
        os.chdir(conf.FONTS_DIR)
        os.system('git pull')
    else:
        os.system(f"git clone {conf.FONTS_REPO} {conf.FONTS_DIR}")
    os.system(f"cp -rf {conf.FONTS_DIR} {conf.FONTS_SYS_DIR}")
    #TODO: sends output to /dev/null
    os.system(f"fc-cache -fv")


def select_branch(branch):
    os.chdir(conf.WEBCOMICS_DIR)
    # switch to branch and update it
    os.system('git fetch')
    os.system(f"git checkout {branch}")
    os.system('git pull')


def detect_episode(branch):
    print(f"detect episode for {branch}")
    r_commits = requests.get(f"{conf.GITLAB_API_URL}/repository/commits?ref_name={branch}")
    if r_commits.status_code == 200:
        for c in r_commits.json():
            r_diff = requests.get(f"{conf.GITLAB_API_URL}/repository/commits/{c['id']}/diff")
            if r_diff.status_code == 200:
                for d in r_diff.json():
                    if '.svg' in d['new_path']:
                        if len(d['new_path'].split('/')) > 2:
                            ep = d['new_path'].split('/')[0]
                            lang = d['new_path'].split('/')[2]
                            return {'episode': ep, 'lang': lang}
                        else:
                            return None


def render_branch(episode, lang):
    os.chdir(f"{conf.WEBCOMICS_DIR}/{episode}")
    os.system(f"rm -rf ./render/{lang}")
    os.system(f"{conf.RENDERFARM_SCRIPT} {lang}")
    

def optimize_images():
    os.system(f"mogrify -background white -flatten -format jpeg {conf.WEBCOMICS_DIR}/*/render/*/*.png")


def generate_render_info(info):
    with open(f"{conf.WEBCOMICS_DIR}/render_info.json", 'w') as f:
        json.dump(info, f)


def create_output_folder(branch, episode, lang):
    os.makedirs(f"{conf.OUTPUT_DIR}/{episode}/{lang}/{branch}")


def generate_episode_from_branch(branch, author):
    try:
        ep = detect_episode(branch)
        if ep is not None and 'episode' in ep and 'lang' in ep:
            select_branch(branch)
            render_branch(ep['episode'], ep['lang'])
            render_info.append(
                {
                    'branch': branch,
                    'author': author,
                    'episode': ep['episode'],
                    'lang': ep['lang']
                }
            )
        else:
            print(f"No episode or lang detected for {branch} - {author}")
    except Exception as e:
        print(f"Unexpected exception while generation {branch} - {author}")
        print(f"{str(e)}")


if __name__ == "__main__" :
    update_repo()
    update_font()
    render_info = []
    merge_requests = get_opened_merged_request()
    if len(merge_requests) > 0:
        for mr in merge_requests:
            generate_episode_from_branch(mr['branch'], mr['author'])
    else:
        print("No merge requests open. Generating from master last detected episode")
        generate_episode_from_branch('master', 'unknown')

    optimize_images()
    generate_render_info(render_info)
