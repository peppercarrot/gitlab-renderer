#!/usr/bin/python3
import json, os, glob, shutil
import conf

def get_render_info():
    with open(f"{conf.WEBCOMICS_DIR}/render_info.json", 'r') as filehandle:  
        info = json.load(filehandle)
    return info

def prepare_gallery():
    info = get_render_info()
    if not os.path.exists('./tmp'):
        os.makedirs('./tmp')

    for i in info:
        dest_dir = f"./tmp/{i['branch']}"
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)

        for f in glob.glob(f"{conf.WEBCOMICS_DIR}/{i['episode']}/render/{i['lang']}/*.jpeg") :
            shutil.copy(f, dest_dir)

def update_website_theme():
    if os.path.exists(conf.THEME_PATH):
        current_dir = os.getcwd()
        os.chdir(conf.THEME_PATH)
        os.system('git pull')
        os.chdir(current_dir)
    else:
        os.system(f"git clone {conf.THEME_REPO} {conf.THEME_PATH}")

def generate_website():
    if not os.path.exists('./public'):
        os.makedirs('./public')
    os.system(f"thumbsup --input ./tmp --output public/ --title \"P&C ABC\" --theme-path {conf.THEME_PATH} --sort-media-by filename")

if __name__ == '__main__':
    update_website_theme() 
    prepare_gallery()
    generate_website()

